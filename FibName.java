/*
 Created by Stephanie Benavidez
 Program: Java
 Environment: NetBeans IDE 8.1
 Software, Product, and Embedded Engineering Candidates

 In the programming language of your choice, write a program generating the 
 first n Fibonacci numbers F(n), printing

 "Buzz" when F(n) is divisible by 3.
 "Fizz" when F(n) is divisible by 5.
 "FizzBuzz" when F(n) is divisible by 15.
 "BuzzFizz" when F(n) is prime.
 the value F(n) otherwise.

 Assuming if n is divisible by 15, we'll see:
 Buzz
 Fizz
 FizzBuzz 
 since 15 is divisible by 3,5 and itself

 Assuming n is only printed if none of the above gets printed

 Assuming F0=0, F1=1, F2=1, etc instead of first term being 0

 Verified output with
  http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Fibonacci/fibFormula.html

 */
package fibname;

import java.util.Scanner;

public class FibName {
    
    //returns the sum of n terms from the Fibonacci series
    public static int fibonacci(int n)
    {
        if(n <= 0)
            return 0;
        else if(n==1 || n==2)
            return 1;
        else
            return fibonacci(n-1) + fibonacci(n-2);
    }
    
    //returns whether n is prime or not
    public static Boolean isPrime(int n)
    {
        Boolean prime = true;
        if(n%2==0)
            prime = false;
        else
        {
            for(int i = 3; i < n; i+=2)
            {
                if(n % i == 0){
                    prime = false;
                    break;
                }
            }
        }
        return prime;
    }

    /**
     * prompt the user-->fibonacci-->name
     * @param args
     */
    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        System.out.print("Please enter a value for n: ");
        int n = input.nextInt();
        int f_n = fibonacci(n); //f(n)
        
        int flag=0; //will print f(n) if none of the above was printed
    
        //because 0 and 1 are not prime and because 0>3,5,15
        if(f_n > 1){
        
            if(f_n % 3 == 0){
                System.out.println("Buzz");
                flag=1;
            }
        
            if(f_n % 5 ==0){
                System.out.println("Fizz");
                flag=1;
            }
        
           if(f_n%3==0 && f_n%5==0){
                System.out.println("FizzBuzz");
                flag=1;
            }
        
           if(isPrime(f_n)){
                System.out.println("BuzzFizz");
                flag=1;
            }
        }
        
        if(flag==0)
            System.out.println(f_n);
    }
    
}
